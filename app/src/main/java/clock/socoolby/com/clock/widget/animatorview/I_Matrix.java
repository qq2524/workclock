package clock.socoolby.com.clock.widget.animatorview;

public interface I_Matrix {
    void translation();
    void rotation();
    void scale(float x,float y);

    void resert();
}
