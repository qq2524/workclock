package clock.socoolby.com.clock.todo;

import android.app.Activity;
import android.content.Context;

import java.util.List;

import clock.socoolby.com.clock.net.NetworkManager;
import clock.socoolby.com.clock.net.auth.AuthCallback;
import clock.socoolby.com.clock.net.base.I_ResponseState;
import clock.socoolby.com.clock.net.listener.StateAbleRequestListener;
import clock.socoolby.com.clock.todo.microsoft.bean.todo.TodoEntity;
import e.odbo.data.dsl.query.QBFParameter;

public interface I_TodoSyncService {

    String getServiceName();

    void start(Context applicationContext, NetworkManager networkManager);

    boolean isSignIn();

    void signIn(Activity activity, AuthCallback authCallback);

    void signOut();

    //list
    void list(StateAbleRequestListener<List<TodoEntity>, I_ResponseState> listRequestListener);

    void list(QBFParameter qbfParameter, StateAbleRequestListener<List<TodoEntity>, I_ResponseState> listRequestListener);

    void next(I_ResponseState state,StateAbleRequestListener<List<TodoEntity>, I_ResponseState> listRequestListener);

    //complete
    void completeTodoEntity(String todoId,StateAbleRequestListener<List<TodoEntity>, I_ResponseState> listRequestListener);
}
