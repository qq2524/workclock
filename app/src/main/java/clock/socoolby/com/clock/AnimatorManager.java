package clock.socoolby.com.clock;

import clock.socoolby.com.clock.widget.animatorview.I_Animator;
import clock.socoolby.com.clock.widget.animatorview.animator.BubbleAnimator;
import clock.socoolby.com.clock.widget.animatorview.animator.BubbleCollisionAnimator;
import clock.socoolby.com.clock.widget.animatorview.animator.BubbleWhirlPoolAnimator;
import clock.socoolby.com.clock.widget.animatorview.animator.CarrouselAnimator;
import clock.socoolby.com.clock.widget.animatorview.animator.DotsLineAnimator;
import clock.socoolby.com.clock.widget.animatorview.animator.FireAnimator;
import clock.socoolby.com.clock.widget.animatorview.animator.FireworkAnimator;
import clock.socoolby.com.clock.widget.animatorview.animator.FluorescenceAnimator;
import clock.socoolby.com.clock.widget.animatorview.animator.MagicLineAnimator;
import clock.socoolby.com.clock.widget.animatorview.animator.PhaserBallAnimator;
import clock.socoolby.com.clock.widget.animatorview.animator.RainAnimator;
import clock.socoolby.com.clock.widget.animatorview.animator.SawtoothAnimator;
import clock.socoolby.com.clock.widget.animatorview.animator.SkyAnimator;
import clock.socoolby.com.clock.widget.animatorview.animator.SnakeAnimator;
import clock.socoolby.com.clock.widget.animatorview.animator.SnowAnimator;
import clock.socoolby.com.clock.widget.animatorview.animator.StarFallAnimator;
import clock.socoolby.com.clock.widget.animatorview.animator.VorolayAnimator;
import clock.socoolby.com.clock.widget.animatorview.animator.WaterAnimator;
import clock.socoolby.com.clock.widget.animatorview.animator.Wave3DAnimator;
import clock.socoolby.com.clock.widget.animatorview.animator.WindmillAnimator;
import clock.socoolby.com.clock.widget.animatorview.animator.textanimator.EZLedAnimator;
import clock.socoolby.com.clock.widget.animatorview.animator.textanimator.EvaporateTextAnimator;

public class AnimatorManager {

    public static final String DEFAULT = "NULL";

    int currnetIndex = -1;

    public final static String[] ANIMATOR_NAMES = new String[]{
            StarFallAnimator.NAME,
            SkyAnimator.NAME,
            RainAnimator.NAME,
            BubbleWhirlPoolAnimator.NAME,
            BubbleAnimator.NAME,
            FluorescenceAnimator.NAME,
            BubbleCollisionAnimator.NAME,
            FireworkAnimator.NAME,
            SnakeAnimator.NAME,
            DotsLineAnimator.NAME,
            WaterAnimator.NAME,
            SawtoothAnimator.NAME,
            WindmillAnimator.NAME,
            SnowAnimator.NAME,
            VorolayAnimator.NAME,
            PhaserBallAnimator.NAME,
            CarrouselAnimator.NAME,
            Wave3DAnimator.NAME
    };

    public I_Animator configByName(String name) {
        if (DEFAULT.equalsIgnoreCase(name)) {
            currnetIndex = -1;
            return null;
        }
        currnetIndex = -1;
        for (int index = 0; index < ANIMATOR_NAMES.length; index++) {
            if (ANIMATOR_NAMES[index].equalsIgnoreCase(name)) {
                currnetIndex = index;
                break;
            }
        }
        return getCurrentAnimator();
    }

    public String next() {
        currnetIndex++;
        if (currnetIndex >= ANIMATOR_NAMES.length)
            currnetIndex = -1;
        return getCurrentAnimatorName();
    }

    public String getCurrentAnimatorName(){
        if (currnetIndex == -1)
            return DEFAULT;
        return ANIMATOR_NAMES[currnetIndex];
    }

    private I_Animator getCurrentAnimator() {
        if (currnetIndex == -1)
            return null;
        I_Animator animator = null;
        switch (ANIMATOR_NAMES[currnetIndex]) {
            case StarFallAnimator.NAME:
                animator = new StarFallAnimator();
                break;
            case SkyAnimator.NAME:
                animator = new SkyAnimator();
                break;
            case RainAnimator.NAME:
                animator = new RainAnimator();
                break;
            case SnowAnimator.NAME:
                animator = new SnowAnimator();
                break;
            case BubbleWhirlPoolAnimator.NAME:
                animator = new BubbleWhirlPoolAnimator();
                break;
            case BubbleAnimator.NAME:
                animator = new BubbleAnimator();
                break;
            case FluorescenceAnimator.NAME:
                animator = new FluorescenceAnimator();
                break;
            case BubbleCollisionAnimator.NAME:
                animator = new BubbleCollisionAnimator();
                break;
            case FireworkAnimator.NAME:
                animator = new FireworkAnimator();
                break;
            case DotsLineAnimator.NAME:
                animator = new DotsLineAnimator();
                break;
            case WaterAnimator.NAME:
                animator = new WaterAnimator();
                break;
            case FireAnimator.NAME:
                animator = new FireAnimator();
                break;
            case SawtoothAnimator.NAME:
                animator = new SawtoothAnimator();
                break;
            case WindmillAnimator.NAME:
                animator = new WindmillAnimator();
                break;
            case VorolayAnimator.NAME:
                animator = new VorolayAnimator();
                break;
            case EZLedAnimator.NAME:
                animator = new EZLedAnimator();
                break;
            case EvaporateTextAnimator.NAME:
                animator = new EvaporateTextAnimator();
                break;
            case PhaserBallAnimator.NAME:
                animator = new PhaserBallAnimator();
                break;
            case CarrouselAnimator.NAME:
                animator = new CarrouselAnimator();
                break;
            case Wave3DAnimator.NAME:
                animator = new Wave3DAnimator();
                break;
            case MagicLineAnimator.NAME:
                animator = new MagicLineAnimator();
                break;
            case SnakeAnimator.NAME:
                animator=new SnakeAnimator();
        }
        return animator;
    }
}
