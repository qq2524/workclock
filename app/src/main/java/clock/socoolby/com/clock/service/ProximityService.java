package clock.socoolby.com.clock.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.RequiresApi;

import clock.socoolby.com.clock.ClockApplication;
import clock.socoolby.com.clock.MainActivity;
import clock.socoolby.com.clock.utils.FuncUnit;
import clock.socoolby.com.clock.screen.ScreenManager;

/**
 * Alway zuo,never die.
 * Created by socoolby on 16/04/2017.
 */

public class ProximityService extends Service {
    private final static String TAG = ProximityService.class.getSimpleName();

    public static final String CHANNEL_ID="workclock_channel";

    protected  boolean  isRunning=false;

    private SensorManager mSensorManager;
    private SensorEventListener mSensorListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {
            float[] its = sensorEvent.values;
            //timber.log.Timber.d(String.format("its %f %f %f len:,%d", its[0], its[1], its[2], its.length));
            if (sensorEvent.sensor.getType() == Sensor.TYPE_PROXIMITY) {
                if (FuncUnit.isForeground(ClockApplication.getContext(), MainActivity.class.getName())) {
                    if (its[0] <= 3) {
                        timber.log.Timber.d("Hand stay");
                        if (ScreenManager.isScreenOn()) {
                            ScreenManager.systemLock(ClockApplication.getInstance().getApplicationContext());
                        } else {
                            ScreenManager.systemUnLock();
                        }
                    } else {
                        timber.log.Timber.d("Hand leave...");
                    }
                }
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };


    @Override
    public void onCreate() {
        super.onCreate();
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        Sensor mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        mSensorManager.registerListener(mSensorListener, mSensor, SensorManager.SENSOR_DELAY_FASTEST);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(isRunning)
            return super.onStartCommand(intent, flags, startId);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            initChanel();
        }
        isRunning=true;
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        mSensorManager.unregisterListener(mSensorListener);
        super.onDestroy();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void initChanel(){
        NotificationChannel channel = new NotificationChannel(CHANNEL_ID,"FallDetect",
                NotificationManager.IMPORTANCE_HIGH);

        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.createNotificationChannel(channel);

        Notification notification = new Notification.Builder(getApplicationContext(),CHANNEL_ID).build();
        startForeground(3210, notification);
    }
}
