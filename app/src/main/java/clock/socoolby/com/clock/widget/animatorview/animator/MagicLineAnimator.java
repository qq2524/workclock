package clock.socoolby.com.clock.widget.animatorview.animator;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import java.util.ArrayList;
import java.util.List;

import clock.socoolby.com.clock.widget.animatorview.AbstractCacheDifferenceAnimator;
import clock.socoolby.com.clock.widget.animatorview.I_AnimatorEntry;


//引用自:https://github.com/zhangyuChen1991/MagicLine
public class MagicLineAnimator extends AbstractCacheDifferenceAnimator<MagicLineAnimator.MagicLine, MagicLineAnimator.CorrdinateData> {

    public static final String NAME="MagicLine";

    public MagicLineAnimator() {
        super(1);
    }

    @Override
    public MagicLine createNewEntry() {
        return new MagicLine(width,height);
    }

    public static float[][] demos={{400,400,0.05f,200,200,0.35f},
            {450,450,0.5f,450,450,0.15f},
            {450,450,1.0f,150,150,0.15f},
            {320,320,0.025f,320,80,0.06f},
            {450,450,1.0f,150,150,0.15f},
            {90, 90,0.5f, 450, 450, 0.15f},
            {200, 200, 0.55f, 450, 450, 0.15f},
            {450, 150, 0.15f, 150, 450, 0.05f},
            {200, 500, 0.4f, 500, 200, 0.4f}
    };

    @Override
    protected void initPaint(Paint mPaint) {
        super.initPaint(mPaint);
        mPaint.setAntiAlias(true);
        mPaint.setStrokeWidth(2);
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setAlpha(50);
    }

    public class MagicLine implements I_AnimatorEntry {

        //起点在x、y移动范围
        private float p1XLength = 400, p1YLength = 20, speedP1 = 0.15f;
        private float p2XLength = 20, p2YLength = 400, speedP2 = 0.05f;
        private double angleP1 = 0, angleP2 = 0;
        private int viewWidth, viewHeight;
        //记录移动过的所有点的数据
        private List<CorrdinateData> corrDatas;
        //动画绘制的时间
        private int animDuration = 600;

        public MagicLine(int viewWidth, int viewHeight) {
            this.viewWidth = viewWidth;
            this.viewHeight = viewHeight;
            init();
        }

        @Override
        public void move(int maxWidth, int maxHight) {
            calculate();
            animDuration--;
            if(animDuration==0) {
                animDuration = 600;
                reStartDraw();
            }
        }

        @Override
        public void onDraw(Canvas canvas, Paint mPaint) {
            for (int i = 0; i < corrDatas.size(); i++) {
                CorrdinateData cd = corrDatas.get(i);
                mPaint.setColor(cd.color);
                canvas.drawLine(cd.p1X, cd.p1Y, cd.p2X, cd.p2Y, mPaint);
            }
        }

        @Override
        public void setAnimatorEntryColor(int color) {
              for(CorrdinateData entry:corrDatas)
                  entry.color=color;
        }

        private void init() {
//            Shader shader = new LinearGradient(cd.p1X, cd.p1Y, cd.p2X, cd.p2Y, colors, null, Shader.TileMode.MIRROR);
//            paint.setShader(shader);
            corrDatas = new ArrayList<>();
        }

        /**
         * 开始绘制
         */
        public void reStartDraw() {
            moveToTrashCache(corrDatas);
            corrDatas.clear();

                int demoIndex=rand.nextInt(demos.length);
                p1XLength = demos[demoIndex][0];
                p1YLength = demos[demoIndex][1];
                speedP1 = demos[demoIndex][2];
                p2XLength = demos[demoIndex][3];
                p2YLength = demos[demoIndex][4];
                speedP2 = demos[demoIndex][5];

        }

        /**
         * 计算坐标值
         */
        private void calculate() {
            angleP1 = angleP1 + speedP1;
            angleP2 = angleP2 + speedP2;

            //两个点的位置更新
            float nowP1X = (float) (p1XLength * Math.cos(angleP1) + viewWidth / 2f);
            float nowP1Y = (float) (p1YLength * Math.sin(angleP1) + viewHeight / 2f);
            float nowP2X = (float) (p2XLength * Math.cos(angleP2) + viewWidth / 2f);
            float nowP2Y = (float) (p2YLength * Math.sin(angleP2) + viewHeight / 2f);

            CorrdinateData corrdinataData =revectForTrashCache();
            randomColorIfAble();
            if(corrdinataData!=null)
                corrdinataData.init(nowP1X, nowP1Y, nowP2X, nowP2Y,color);
            else
               corrdinataData=new CorrdinateData(nowP1X, nowP1Y, nowP2X, nowP2Y,color);
            corrDatas.add(corrdinataData);
        }

        /**
         * 设置参数
         */
        public void setParam(float p1XLength, float p1YLength, float p2XLength, float p2YLength, float speedP1, float speedP2) {
            this.p1XLength = p1XLength;
            this.p1YLength = p1YLength;
            this.p2XLength = p2XLength;
            this.p2YLength = p2YLength;
            this.speedP1 = speedP1;
            this.speedP2 = speedP2;
        }
    }

    public class CorrdinateData {
        float p1X, p1Y, p2X, p2Y;
        int color;

        CorrdinateData(float p1X, float p1Y, float p2X, float p2Y,int color) {
            init(p1X,p1Y,p2X,p2Y,color);
        }

        public void init(float p1X, float p1Y, float p2X, float p2Y,int color) {
            this.p1X = p1X;
            this.p1Y = p1Y;
            this.p2X = p2X;
            this.p2Y = p2Y;
            this.color=color;
        }
    }
}
