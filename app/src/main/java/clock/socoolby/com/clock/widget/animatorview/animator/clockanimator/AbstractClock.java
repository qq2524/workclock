package clock.socoolby.com.clock.widget.animatorview.animator.clockanimator;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;

import java.util.Calendar;

import clock.socoolby.com.clock.widget.animatorview.I_AnimatorEntry;
import clock.socoolby.com.clock.widget.animatorview.animator.clockanimator.pointer.DefaultPointer;

public abstract class AbstractClock implements I_AnimatorEntry {
    protected int width,hight;

    protected int mCenterX, mCenterY;

    // 默认刻度画笔、文字画笔；
    protected Paint mDefaultPaint, mTextPaint;

    // 时钟半径、中心点半径、默认刻度长度、默认刻度宽度、特殊刻度长度、特殊刻度宽度
    protected float mRadius,
            mDefaultScaleLength, mDefaultScaleWidth,
            mParticularlyScaleLength, mParticularlyScaleWidth;

    // 当前时、分、秒
    protected int mH, mM, mS;

    // 时钟颜色、默认刻度颜色、时刻度颜色
    protected int mClockColor, mColorDefaultScale, mColorParticularyScale,textColor,mClockColorPointer,pointerSecondColor;

    I_Pointer pointer;

    SimulateTextShowTypeEnum simulateTextShowTypeEnum=SimulateTextShowTypeEnum.FOUR;


    public AbstractClock() {

    }

    public void init(int  width,int hight,int mCenterX, int mCenterY, float mRadius, int mClockColor,int textColor){
        this.width=width;
        this.hight=hight;
        this.mCenterX = mCenterX;
        this.mCenterY = mCenterY;
        this.mRadius = mRadius;
        this.mClockColor = mClockColor;
        this.textColor=textColor;
        initScaleLength();
        init();
        if(pointer==null)
            setPointer(new DefaultPointer());
        else
            pointer.init(mRadius,mClockColor);

        Log.d("clock","start type:"+typeName()+" \tw:"+width+"\th:"+hight+"\tcX:"+mCenterX+"\tcY:"+mCenterY);
    }

    private void init() {
        mColorDefaultScale =  mClockColor;
        mColorParticularyScale =  mClockColor;
        mClockColorPointer=mClockColor;
        pointerSecondColor=mClockColor;

        mDefaultPaint = new Paint();
        mDefaultPaint.setAntiAlias(true);
        mDefaultPaint.setStyle(Paint.Style.STROKE);

        mTextPaint = new Paint();
        mTextPaint.setStrokeWidth(mDefaultScaleWidth / 2);
        mTextPaint.setTextSize(mParticularlyScaleWidth * 4);
        mTextPaint.setTextAlign(Paint.Align.CENTER);
        //mTextPaint.setParticleColor(textColor);
    }


    protected void initScaleLength(){
        /*
         * 默认时钟刻度长=半径/10;
         * 默认时钟刻度宽=长/6;
         *
         * */
        mDefaultScaleLength = mRadius / 10;
        mDefaultScaleWidth = mDefaultScaleLength / 6;

        /*
         * 特殊时钟刻度长=半径/5;
         * 特殊时钟刻度宽=长/6;
         *
         * */
        mParticularlyScaleLength = mRadius / 5;
        mParticularlyScaleWidth = mParticularlyScaleLength / 6;
    }


    public  void setPointer(I_Pointer pointer){
        this.pointer=pointer;
        pointer.init(mRadius,mClockColor);
        pointer.setmPointerColor(mClockColorPointer);
        pointer.setColorSecond(pointerSecondColor);
    }

    @Override
    public void setAnimatorEntryColor(int color) {
        mClockColor=color;
        pointer.init(mRadius,mClockColor);
    }

    public int getmColorDefaultScale() {
        return mColorDefaultScale;
    }

    public void setmColorDefaultScale(int mColorDefaultScale) {
        this.mColorDefaultScale = mColorDefaultScale;
    }

    public int getmColorParticularyScale() {
        return mColorParticularyScale;
    }

    public void setmColorParticularyScale(int mColorParticularyScale) {
        this.mColorParticularyScale = mColorParticularyScale;
    }

    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }

    public int getmClockColor() {
        return mClockColor;
    }

    public void setmClockColor(int mClockColor) {
        this.mClockColor = mClockColor;
    }

    public int getmClockColorPointer() {
        return mClockColorPointer;
    }

    public void setmClockColorPointer(int mClockColorPointer) {
        this.mClockColorPointer = mClockColorPointer;
        pointer.setmPointerColor(mClockColorPointer);
    }

    public int getPointerSecondColor() {
        return pointerSecondColor;
    }

    public void setPointerSecondColor(int pointerSecondColor) {
        this.pointerSecondColor = pointerSecondColor;
        pointer.setColorSecond(pointerSecondColor);
    }

    public SimulateTextShowTypeEnum getSimulateTextShowTypeEnum() {
        return simulateTextShowTypeEnum;
    }

    public void setSimulateTextShowTypeEnum(SimulateTextShowTypeEnum simulateTextShowTypeEnum) {
        this.simulateTextShowTypeEnum = simulateTextShowTypeEnum;
    }

    /**
     * 获取当前系统时间
     */
    private void getTime() {
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR);
        hour = hour % 12;
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);

        if (hour != mH || minute != mM || second != mS) {
            setTime(hour, minute, second);
        }
    }

    /**
     * 设置时间
     */
    private void setTime(int h, int m, int s) {
        mH = h;
        mM = m;
        mS = s;
    }


    public void onDraw(Canvas canvas, Paint mPaint) {

        drawBorder(canvas);

        drawOrnament(canvas);
        // 坐标原点移动到View 中心
        canvas.translate(mCenterX, mCenterY);
        mTextPaint.setColor(textColor);
        drawText(canvas);
        mPaint.setColor(mClockColorPointer);
        pointer.drawPointer(canvas,mH,mM,mS,mPaint);
    }

    /**
     * 绘制 装饰
     * @param canvas
     */
    protected int loop=0;
    private char[] mark=new char[]{'w','s','l'};
    private int dealy=0;
    private char c;
    protected void drawOrnament(Canvas canvas){
        Paint.FontMetrics fontMetrics = mTextPaint.getFontMetrics();

        // 文字顶部与基线距离
        float ascent = Math.abs(fontMetrics.ascent);
        // 文字底部与基线距离
        float descent = Math.abs(fontMetrics.descent);
        // 文字高度
        float fontHeight = ascent + descent;

        // 文字宽度
        float fontWidth=mTextPaint.measureText("w");

        // 开始XX
        float x = mCenterX-(fontWidth*mark.length)/2+fontWidth/2;

        // y轴坐标为: -（半径-特殊刻度长度-特殊刻度宽度（作为间距）-文字顶部距离基线的距离）
        float y = mParticularlyScaleLength +fontHeight * 2;

        for(int i=0;i<mark.length;i++) {
            c=mark[i];
            if(i==loop)
                c=Character.toUpperCase(c);
            canvas.drawText(String.valueOf(c), x+fontWidth*i, y, mTextPaint);
        }
        if(dealy++>200) {
            dealy=0;
            loop++;
        }
        if(loop==mark.length)
            loop=0;
    }

    /**
     * 绘制时钟的圆形和刻度
     */
    protected abstract void drawBorder(Canvas canvas);


    /**
     * 绘制特殊时刻（12点、3点、6点、9点)的文字
     */
    protected  void drawText(Canvas canvas) {

        Paint.FontMetrics fontMetrics = mTextPaint.getFontMetrics();

        // 文字顶部与基线距离
        float ascent = Math.abs(fontMetrics.ascent);
        // 文字底部与基线距离
        float descent = Math.abs(fontMetrics.descent);
        // 文字高度
        float fontHeight = ascent + descent;
        // 文字竖直中心点距离基线的距离；
        float offsetY = fontHeight / 2 - Math.abs(fontMetrics.descent);
        // 文字宽度
        float fontWidth;

        // drawText(@NonNull String text, float x, float y, @NonNull Paint paint) 参数：y，为基线的y坐标，并非文字左下角的坐标
        // 文字距离圆圈的距离为 特殊刻度长度+宽度

        String h = "12";
        // y轴坐标为: -（半径-特殊刻度长度-特殊刻度宽度（作为间距）-文字顶部距离基线的距离）
        float y = -(mRadius - mParticularlyScaleLength - mParticularlyScaleWidth - ascent);
        canvas.drawText(h, 0, y, mTextPaint);


        h = "3";
        fontWidth = mTextPaint.measureText(h);
        // y轴坐标为: 半径-特殊刻度长度-特殊刻度宽度（作为间距）-文字长度/2（绘制原点在文字横向中心）
        y = mRadius - mParticularlyScaleLength - mParticularlyScaleWidth - (fontWidth / 2);
        canvas.drawText(h, y, 0 + offsetY, mTextPaint);

        h = "6";
        // y轴坐标为: 半径-特殊刻度长度-特殊刻度宽度（作为间距）-文字底部与基线的距离
        y = mRadius - mParticularlyScaleLength - mParticularlyScaleWidth - descent;
        canvas.drawText(h, 0, y, mTextPaint);

        h = "9";
        fontWidth = mTextPaint.measureText(h);
        // y轴坐标为: -（半径-特殊刻度长度-特殊刻度宽度（作为间距）-文字长度/2（绘制原点在文字横向中心））
        y = -(mRadius - mParticularlyScaleLength - mParticularlyScaleWidth - (fontWidth / 2));

        canvas.drawText(h, y, 0 + offsetY, mTextPaint);
    }

    @Override
    public void move(int maxWidth, int maxHight) {
        getTime();
    }

    public abstract String typeName();
}
