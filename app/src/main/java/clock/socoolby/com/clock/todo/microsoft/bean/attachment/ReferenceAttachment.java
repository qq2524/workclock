package clock.socoolby.com.clock.todo.microsoft.bean.attachment;

/**
 *
 * isFolder	Boolean	指定附件是否为文件夹的链接。 如果sourceUrl是指向文件夹的链接, 则必须将其设置为 true。 可选。
 * permission	referenceAttachmentPermission	指定通过providerType中的提供程序类型授予附件的权限。 可取值为：other、view、edit、anonymousView、anonymousEdit、organizationView 或 organizationEdit。 可选。
 * previewUrl	String	仅适用于图像 URL 的引用附件, 以获取预览图像。 仅当sourceUrl标识图像文件时, 才使用thumbnailUrl和previewUrl 。 可选。
 * providerType	: Referenceattachmentprovider	支持此 contentType 的附件的提供程序的类型。 可取值为：other、oneDriveBusiness、oneDriveConsumer、dropbox。 可选。
 * sourceUrl	String	用于获取附件内容的 URL。 如果这是指向文件夹的 URL, 然后在 Outlook 或 web 上的 Outlook 中正确显示该文件夹, 请将isFolder设置为 true。 必需。
 * thumbnailUrl	String	仅适用于图像 URL 的引用附件, 以获取缩略图图像。 仅当sourceUrl标识图像文件时, 才使用thumbnailUrl和previewUrl 。 可选。
 *
 */

public class ReferenceAttachment extends Attachment {
        Boolean isFolder;
        String permission;
        String previewUrl;
        String providerType;
        String sourceUrl;
        String thumbnailUrl;

}

