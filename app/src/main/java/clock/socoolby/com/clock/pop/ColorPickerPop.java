package clock.socoolby.com.clock.pop;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import com.larswerkman.holocolorpicker.ColorPicker;
import com.larswerkman.holocolorpicker.OpacityBar;
import com.larswerkman.holocolorpicker.SVBar;
import clock.socoolby.com.clock.R;
import razerdp.basepopup.BasePopupWindow;

/**
 * 颜色选择器
 * Created by tianzl on 2017/9/1.
 */

public class ColorPickerPop extends BasePopupWindow implements View.OnClickListener {
    private ImageButton ibBack;
    private ImageButton ibEnsure;
    private TextView tvTitle;
    private ColorPicker colorPicker;
    private SVBar svBar;
    private OpacityBar opacityBar;

    private int color;

    public ColorPickerPop(Context context) {
        super(context);
    }

    public View onCreateContentView() {
        View content=createPopupById(R.layout.pop_color_picker);
        initView(content);
        initData();
        initEvent();
        return content;
    }

    private void initData() {
        colorPicker.addSVBar(svBar);
        colorPicker.addOpacityBar(opacityBar);
        tvTitle.setText("颜色选择器");
    }

    private void initView(View view) {
        ibBack=view.findViewById(R.id.bar_title_left);
        ibEnsure=view.findViewById(R.id.bar_title_right);
        tvTitle=view.findViewById(R.id.bar_title_title);
        colorPicker=view.findViewById(R.id.dialog_color_picker_colorPicker);
        svBar=view.findViewById(R.id.dialog_color_picker_svbar);
        opacityBar=view.findViewById(R.id.dialog_color_picker_opacity);

        view.findViewById(R.id.default_color_sample).setOnClickListener(this);
        view.findViewById(R.id.default_color_sample1).setOnClickListener(this);
        view.findViewById(R.id.default_color_sample2).setOnClickListener(this);
        view.findViewById(R.id.default_color_sample3).setOnClickListener(this);
        view.findViewById(R.id.default_color_sample4).setOnClickListener(this);
        view.findViewById(R.id.default_color_sample5).setOnClickListener(this);
        view.findViewById(R.id.default_color_sample6).setOnClickListener(this);
        view.findViewById(R.id.default_color_sample7).setOnClickListener(this);
        view.findViewById(R.id.default_color_sample8).setOnClickListener(this);
        view.findViewById(R.id.default_color_sample9).setOnClickListener(this);
        view.findViewById(R.id.default_color_sample10).setOnClickListener(this);
        view.findViewById(R.id.default_color_sample11).setOnClickListener(this);
        view.findViewById(R.id.default_color_sample12).setOnClickListener(this);
        view.findViewById(R.id.default_color_sample13).setOnClickListener(this);
        view.findViewById(R.id.default_color_sample14).setOnClickListener(this);

    }

    private void initEvent() {
        colorPicker.setOnColorChangedListener(new ColorPicker.OnColorChangedListener() {
            @Override
            public void onColorChanged(int colors) {
                tvTitle.setTextColor(colors);
                color=colors;
            }
        });
        ibBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onBack();
                dismiss();
            }
        });
        ibEnsure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onEnsure(color);
                dismiss();
            }
        });
    }

    public void show(int initColor) {
        colorPicker.setColor(initColor);
        color=initColor;
        tvTitle.setTextColor(initColor);
        this.showPopupWindow();
    }

    @Override
    public void onClick(View v) {
        ColorDrawable colordDrawable = (ColorDrawable) v.getBackground();
        int colors = colordDrawable.getColor();
        tvTitle.setTextColor(colors);
        color=colors;
    }

    public interface OnColorListener{
        void onBack();
        void onEnsure(int color);
    }

    private OnColorListener listener;
    public void setOnColorChangeListenter(OnColorListener listener){
        this.listener=listener;
    }
}
