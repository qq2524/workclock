package clock.socoolby.com.clock.pop;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import clock.socoolby.com.clock.R;
import clock.socoolby.com.clock.net.protocol.weather.WeatherResponse;
import clock.socoolby.com.clock.net.protocol.weather.bean.Weather;
import razerdp.basepopup.BasePopupWindow;

public class WeatherPopup extends BasePopupWindow {
    private TextView weatherTitle;
    private TextView weatherTodayTextView;
    private TextView weatherToday1TextView;
    private TextView weatherToday2TextView;
    private TextView weatherToday3TextView;
    private ImageView weatherTodayImageView;
    private ImageView weatherToday1ImageView;
    private ImageView weatherToday2ImageView;
    private ImageView weatherToday3ImageView;


    public WeatherPopup(Context context) {
        super(context);
    }

    @Override
    public View onCreateContentView() {
        View content=createPopupById(R.layout.pop_weather);
        weatherTitle=content.findViewById(R.id.tv_weather_title_city);
        content.findViewById(R.id.tv_weather_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        weatherTodayTextView=content.findViewById(R.id.tv_weather_today_textView);
        weatherToday1TextView=content.findViewById(R.id.tv_weather_today1_textView);
        weatherToday2TextView=content.findViewById(R.id.tv_weather_today2_textView);
        weatherToday3TextView=content.findViewById(R.id.tv_weather_today3_textView);

        weatherTodayImageView=content.findViewById(R.id.tv_weather_today_image);
        weatherToday1ImageView=content.findViewById(R.id.tv_weather_today1_image);
        weatherToday2ImageView=content.findViewById(R.id.tv_weather_today2_image);
        weatherToday3ImageView=content.findViewById(R.id.tv_weather_today3_image);
        return content;
    }

    public void init(List<Weather> weatherList, String title){
        weatherTitle.setText(title);
        RequestOptions options = new RequestOptions();
        if(weatherList.size()>0){
            Weather weather=weatherList.get(0);
            String text=weather.toString();
            weatherTodayTextView.setText(text);
            options.override(400);
            Glide.with(getContext()).load(Uri.parse(weather.dayPictureUrl)).apply(options).into(weatherTodayImageView);

            weather=weatherList.get(1);
            text=weather.toString();
            weatherToday1TextView.setText(text);
            options.override(100);
            Glide.with(getContext()).load(Uri.parse(weather.dayPictureUrl)).apply(options).into(weatherToday1ImageView);

            weather=weatherList.get(2);
            text=weather.toString();
            weatherToday2TextView.setText(text);
            Glide.with(getContext()).load(Uri.parse(weather.dayPictureUrl)).apply(options).into(weatherToday2ImageView);

            weather=weatherList.get(3);
            text=weather.toString();
            weatherToday3TextView.setText(text);
            Glide.with(getContext()).load(Uri.parse(weather.dayPictureUrl)).apply(options).into(weatherToday3ImageView);
        }
    }
}
