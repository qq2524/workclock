package clock.socoolby.com.clock;

import android.util.Log;

import com.openbravo.data.basic.BasicException;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.List;
import java.util.UUID;

import clock.socoolby.com.clock.dao.base.ThemeUI;
import clock.socoolby.com.clock.model.DigitPerferenceModel;
import clock.socoolby.com.clock.model.SharePerferenceModel;
import clock.socoolby.com.clock.model.SimulatePerferenceModel;
import clock.socoolby.com.clock.state.ClockInterfaceTypeEnum;
import clock.socoolby.com.clock.state.ClockThemeUITypeEnum;
import e.odbo.data.dao.EntityManager;
import e.odbo.data.dsl.query.QBF;

import static clock.socoolby.com.clock.state.ClockThemeUITypeEnum.FULLSCREEN;

public class ThemeUIManager{

    public static final String TAG = ThemeUIManager.class.getSimpleName();

    EntityManager entityManager;

    SharePerferenceModel model;

    FontManager fontManager;

    private String tempThemeStr=null;

    public ThemeUIManager(EntityManager entityManager,SharePerferenceModel model,FontManager fontManager) {
        this.entityManager = entityManager;
        this.model=model;
        this.fontManager=fontManager;
    }

    public void loadDigitThemeFromDB(String name) throws BasicException {
        ThemeUI themeUI=findThemeUIFromDB(name,ClockInterfaceTypeEnum.Digit.code);
        if(themeUI!=null) {
            loadDigitTheme(themeUI.getConfigText());
        }
    }

    public void loadDigitTheme(String jsonString) throws BasicException {
            DigitPerferenceModel digitPerferenceModel=new DigitPerferenceModel();
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(jsonString);
                digitPerferenceModel.fromJsonString(jsonObject);
                model.themeUILoad(jsonObject);
                model.setDigitPerferenceModel(digitPerferenceModel);
            } catch (JSONException e) {
                throw new BasicException(e);
            }
    }

    public void loadSimulateThemeFromDB(String name)throws BasicException {
        ThemeUI themeUI=findThemeUIFromDB(name,ClockInterfaceTypeEnum.Simulate.code);
        if(themeUI!=null) {
            loadSimulateTheme(themeUI.getConfigText());
        }
    }

    public void loadSimulateTheme(String jsonString)throws BasicException {
        SimulatePerferenceModel simulatePerferenceModel=new SimulatePerferenceModel();
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(jsonString);
            simulatePerferenceModel.fromJsonString(jsonObject);
            model.themeUILoad(jsonObject);
            model.setSimulatePerferenceModel(simulatePerferenceModel);
        } catch (JSONException e) {
            throw new BasicException(e);
        }
    }

    public void saveDigitThemeFromModel(String name)throws BasicException {
        ThemeUI themeUI=findThemeUIFromDB(name,ClockInterfaceTypeEnum.Digit.code);
        if(themeUI==null){
            themeUI=new ThemeUI();
            themeUI.setId(UUID.randomUUID().toString());
            themeUI.setName(name);
            themeUI.setClockType(ClockInterfaceTypeEnum.Digit.code);
            themeUI.setConfigText(makeSaveDigitThemeString(model.getDigitPerferenceModel()));
            entityManager.insert(themeUI);
        }else{
            themeUI.setConfigText(makeSaveDigitThemeString(model.getDigitPerferenceModel()));
            entityManager.update(themeUI);
        }
    }

    public String makeSaveDigitThemeString(DigitPerferenceModel digitPerferenceModel){
        JSONObject jsonObject = new JSONObject();
        try {
            digitPerferenceModel.toJsonString(jsonObject);
            model.themeUISave(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    public void saveSimulateThemeFromModel(String name)throws BasicException {
        ThemeUI themeUI=findThemeUIFromDB(name,ClockInterfaceTypeEnum.Simulate.code);
        if(themeUI==null){
            themeUI=new ThemeUI();
            themeUI.setId(UUID.randomUUID().toString());
            themeUI.setName(name);
            themeUI.setClockType(ClockInterfaceTypeEnum.Simulate.code);
            themeUI.setConfigText(makeSaveSimulateThemeString(model.getSimulatePerferenceModel()));
            entityManager.insert(themeUI);
        }else{
            themeUI.setConfigText(makeSaveSimulateThemeString(model.getSimulatePerferenceModel()));
            entityManager.update(themeUI);
        }
    }

    public String makeSaveSimulateThemeString(SimulatePerferenceModel simulatePerferenceModel){
        JSONObject jsonObject = new JSONObject();
        try {
            simulatePerferenceModel.toJsonString(jsonObject);
            model.themeUISave(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    private ThemeUI findThemeUIFromDB(String name,int type)throws BasicException{
        ThemeUI themeUI=null;
        if(!exitsThemeUIStyle(type,name))
            return null;
        List<ThemeUI> themeUIList=entityManager.query(ThemeUI.class, QBF.c().eq(ThemeUI.NAME,name).eq(ThemeUI.CLOCK_TYPE, type).build());
        if(themeUIList.size()>0)
            themeUI=themeUIList.get(0);
        return themeUI;
    }

    public boolean exitsThemeUIStyle(int type,String name)throws BasicException{
        return entityManager.isExistByFields(ThemeUI.class,new String[]{ThemeUI.NAME,ThemeUI.CLOCK_TYPE},new Object[]{name,type});
    }

    public void saveTempThemeUI(int type){
        timber.log.Timber.d("save temp ThemeUI type:"+type);
        if(type==ClockInterfaceTypeEnum.Digit.code)
          this.tempThemeStr=makeSaveDigitThemeString(model.getDigitPerferenceModel());
        else
          this.tempThemeStr=makeSaveSimulateThemeString(model.getSimulatePerferenceModel());
    }

    public void recoverTempThemeUI(int type)throws BasicException{
        timber.log.Timber.d("recover temp ThemeUI type"+type);
        if(tempThemeStr==null)
            return;
        if(type==ClockInterfaceTypeEnum.Digit.code) {
            loadDigitTheme(tempThemeStr);
        }
        else {
            loadSimulateTheme(tempThemeStr);
        }
    }

    public void loadThemeFromNet(ThemeUI netTheme)throws BasicException{
        if(netTheme==null)
            return;
        int clockType=netTheme.getClockType();
        if(clockType==ClockInterfaceTypeEnum.Digit.code) {
            loadDigitTheme(netTheme.getConfigText());
            fontManager.updateFontSize(model.getFontName(),isFullScreen(), model.isDisplaySecond(), model.getFontStyleSize());
        }else if(clockType==ClockInterfaceTypeEnum.Simulate.code)
            loadSimulateTheme(netTheme.getConfigText());
        model.setClockInterfaceType(clockType);
    }

    private boolean isFullScreen() {
        return ClockThemeUITypeEnum.valueOf(model.getThemeUIType()) == FULLSCREEN;
    }
}
