package clock.socoolby.com.clock.widget.animatorview.animator.model.trajectory;


import clock.socoolby.com.clock.widget.animatorview.I_SpeedInterpolator;
import clock.socoolby.com.clock.widget.animatorview.I_Trajectory;

public abstract class AbstractTrajectory implements I_Trajectory {
    protected I_SpeedInterpolator speedInterpolator;

    public AbstractTrajectory(I_SpeedInterpolator speedInterpolator) {
        this.speedInterpolator = speedInterpolator;
    }

    public I_SpeedInterpolator getSpeedInterpolator() {
        return speedInterpolator;
    }

    public void setSpeedInterpolator(I_SpeedInterpolator speedInterpolator) {
        this.speedInterpolator = speedInterpolator;
    }
}
