package clock.socoolby.com.clock.todo.microsoft.bean.todo;

import org.json.JSONException;
import org.json.JSONObject;

import clock.socoolby.com.clock.todo.microsoft.bean.AbstractChangeLogMircsoftEntity;


public class TodoGroup extends AbstractChangeLogMircsoftEntity {

    public static final String GROUPKEY="groupKey";
    private String groupkey;
    public static final String ISDEFAULTGROUP="isDefaultGroup";
    private String isdefaultgroup;
    private String name;
    public void setGroupkey(String groupkey) {
        this.groupkey = groupkey;
    }
    public String getGroupkey() {
        return groupkey;
    }

    public void setIsdefaultgroup(String isdefaultgroup) {
        this.isdefaultgroup = isdefaultgroup;
    }
    public String getIsdefaultgroup() {
        return isdefaultgroup;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    @Override
    public void fromJson(JSONObject jsonObject) throws JSONException {
        super.fromJson(jsonObject);
    }

    @Override
    public void toJson(JSONObject jsonObject) throws JSONException {

    }
}
