package clock.socoolby.com.clock.widget.animatorview.animator.clockanimator.pointer;

import android.graphics.Canvas;
import android.graphics.Path;

public class SwordPointer extends SecondTailPointer{

    @Override
    public void initPointerLength(float mRadius) {
        super.initPointerLength(mRadius);
        mHourPointerWidth=mHourPointerWidth/4;
        mMinutePointerWidth=mMinutePointerWidth/4;
    }

    @Override
    protected void drawHourPointer(Canvas canvas) {
        mPointerPaint.setStrokeWidth(mHourPointerWidth);
        mPointerPaint.setColor(mColorHourPointer);

        // 当前时间的总秒数
        float s = mH * 60 * 60 + mM * 60 + mS;
        // 百分比
        float percentage = s / (12 * 60 * 60);
        // 通过角度计算弧度值，因为时钟的角度起线是y轴负方向，而View角度的起线是x轴正方向，所以要加270度
        float angle = calcAngle(percentage);

        float x = calcX(mHourPointerLength , angle);
        float y = calcY(mHourPointerLength , angle);

        float minLength=mHourPointerLength*4/5;

        float x3 = calcX(minLength , angle+10);
        float y3 = calcY(minLength , angle+10);

        float x4 = calcX(minLength , angle-10);
        float y4 = calcY(minLength , angle-10);

        minLength=mHourPointerLength/5;

        float x1=calcX(minLength , angle+180+20);
        float y1 = calcY(minLength , angle+180+20);

        float x2=calcX(minLength, angle+180-20);
        float y2 = calcY(minLength , angle+180-20);


        Path path=new Path();
        path.lineTo(x1,y1);
        path.lineTo(x2,y2);
        path.lineTo(0,0);
        path.lineTo(x3,y3);
        path.lineTo(x,y);
        path.lineTo(x4,y4);
        path.lineTo(0,0);

        canvas.drawPath(path,mPointerPaint);
    }

    @Override
    protected void drawMinutePointer(Canvas canvas) {
        mPointerPaint.setStrokeWidth(mMinutePointerWidth);
        mPointerPaint.setColor(mColorMinutePointer);

        float s = mM * 60 + mS;
        float percentage = s / (60 * 60);
        float angle = calcAngle(percentage);

        float x = calcX(mMinutePointerLength , angle);
        float y = calcY(mMinutePointerLength , angle);

        float minLength=mMinutePointerLength*4/5;

        float x3 = calcX(minLength , angle+6);
        float y3 = calcY(minLength , angle+6);

        float x4 = calcX(minLength , angle-6);
        float y4 = calcY(minLength , angle-6);

        minLength=mMinutePointerLength/5;

        float x1=calcX(minLength , angle+180+10);
        float y1 = calcY(minLength , angle+180+10);

        float x2=calcX(minLength, angle+180-10);
        float y2 = calcY(minLength , angle+180-10);


        Path path=new Path();
        path.lineTo(x1,y1);
        path.lineTo(x2,y2);
        path.lineTo(0,0);
        path.lineTo(x3,y3);
        path.lineTo(x,y);
        path.lineTo(x4,y4);
        path.lineTo(0,0);

        canvas.drawPath(path,mPointerPaint);
    }

    public static final String TYPE_SWORD_POINTER="SwordPointer";


    @Override
    public String typeName() {
        return TYPE_SWORD_POINTER;
    }
}
