package clock.socoolby.com.clock;

import android.app.Application;
import android.content.Context;

import com.blankj.utilcode.constant.PermissionConstants;
import com.blankj.utilcode.util.PermissionUtils;
import com.blankj.utilcode.util.Utils;
import com.openbravo.data.basic.BasicException;
import com.openbravo.data.loader.DBSession;
import com.openbravo.data.loader.TableDBSentenceBuilder;
import com.openbravo.data.loader.dialect.SQLite.SQLite;

import clock.socoolby.com.clock.dao.base.ThemeUIDao;
import clock.socoolby.com.clock.dao.base.TimeFontStyleDao;
import clock.socoolby.com.clock.db.WorkClockDatabase;
import clock.socoolby.com.clock.model.DateModel;
import clock.socoolby.com.clock.net.BusinessService;
import clock.socoolby.com.clock.net.NetworkManager;
import clock.socoolby.com.clock.utils.FileUtils;
import clock.socoolby.com.clock.model.SharePerferenceModel;
import e.odbo.DB;
import e.odbo.data.dao.EntityManager;
import e.odbo.data.sample.security.NoScuritySupportManager;
import timber.log.Timber;

public class ClockApplication extends Application {

    private static ClockApplication sEndzoneBoxApp;
    private BusinessService mBusinessService;

    public static ClockApplication getInstance() {
        return sEndzoneBoxApp;
    }

    SharePerferenceModel model;

    EntityManager entityManager;

    @Override
    public void onCreate() {
        super.onCreate();
        sEndzoneBoxApp = this;
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new CrashReportingTree());
        }
        Utils.init(this);
        init();
        initEnityManager();
    }

    private void initEnityManager() {
        DB localDB=null;
        WorkClockDatabase database=new WorkClockDatabase();
        try {
            localDB= SQLite.androidWithSQLDroid(this.getPackageName(),Constants.APP_NAME,null,null);
            timber.log.Timber.d("setup database start....");
            localDB.setup(database);
        }catch (BasicException e){
            timber.log.Timber.e(e,"setup database false");
        }
        localDB.model(database);
        timber.log.Timber.d("start database end....");
        DBSession dbSession=(DBSession) localDB.getDbSession();
        entityManager=new EntityManager(dbSession,new TableDBSentenceBuilder(dbSession,new NoScuritySupportManager()));
        entityManager.addDaoToMap(new TimeFontStyleDao(dbSession));
        entityManager.addDaoToMap(new ThemeUIDao(dbSession));
    }


    public void init() {
        PermissionUtils.permission(PermissionConstants.SENSORS, PermissionConstants.STORAGE)
                .rationale(new PermissionUtils.OnRationaleListener(){
                    @Override
                    public void rationale(ShouldRequest shouldRequest) {
                        //DialogHelper.showRationaleDialog(shouldRequest);
                    }
                }).callback(new PermissionUtils.SimpleCallback() {
            @Override
            public void onGranted() {
                Timber.d("supported permission....");
            }

            @Override
            public void onDenied() {
                Timber.d("supported permission denied...");
            }
        }).request();
        model = new SharePerferenceModel();
        if (!FileUtils.isExistsFile(Constants.SHARE_PERFERENCE_FILE)) {
            model.setTypeHourPower(Constants.TALKING_HOURS);
            DateModel startTimeModel = new DateModel();
            startTimeModel.setTime(12, 31);
            DateModel stopTimeModel = new DateModel();
            stopTimeModel.setTime(14, 31);
            model.setStartHourPowerTime(startTimeModel);
            model.setStopHourPowerTime(stopTimeModel);
        }else
            model.read();
    }

    public static Context getContext() {
        return ClockApplication.getInstance().getApplicationContext();
    }

    public BusinessService getBusinessService() {
        if(mBusinessService==null) {
            mBusinessService = new BusinessService(NetworkManager.getInstance(getApplicationContext()));
        }
        return mBusinessService;
    }

    public SharePerferenceModel getModel() {
        return model;
    }

    public EntityManager getEntityManager(){
        return entityManager;
    }

    private static class CrashReportingTree extends Timber.Tree {
        @Override
        protected void log(int priority, String tag, String message, Throwable t) {
        }
    }
}
