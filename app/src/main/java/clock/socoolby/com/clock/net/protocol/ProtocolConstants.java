package clock.socoolby.com.clock.net.protocol;

public class ProtocolConstants {

    public static final String STR_RESULT_CODE = "code";
    public static final String STR_RESULT_DES = "msg";

    public static final int RESULT_OK = 0;
    public static final int RESULT_FAILED = 1;
    public static final int RESULT_NETWORK_ERROR = -2;

}
