package clock.socoolby.com.clock.todo.microsoft.query;


import com.openbravo.data.loader.serialize.DataWriteUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import clock.socoolby.com.clock.todo.microsoft.utils.TypeUtils;
import e.odbo.data.dsl.query.QBFCompareEnum;
import e.odbo.data.dsl.query.QBFParameter;


public class QBFBuilder {
    public static final String START_PREFIX="?";

    public static final String FILTER_PREFIX="$filter=";

    public static final String ORDER_PREFIX="$orderby=";

    public static final String LINK_PREFIX="&";


    public static final Map<QBFCompareEnum,String> transformMap=new HashMap<>();

    {
        transformMap.put(QBFCompareEnum.COMP_EQUALS," eq ");
        transformMap.put(QBFCompareEnum.COMP_DISTINCT," ne ");
        transformMap.put(QBFCompareEnum.COMP_GREATER," gt ");
        transformMap.put(QBFCompareEnum.COMP_GREATEROREQUALS," ge ");
        transformMap.put(QBFCompareEnum.COMP_LESS," lt ");
        transformMap.put(QBFCompareEnum.COMP_LESSOREQUALS," le ");

    }

    public static String getFilterStr(QBFParameter queryParameter){
        String retStr=qbfParameterFunc(queryParameter.getFieldName(),queryParameter.getQbf(),passQbfParaValue(queryParameter.getValue()));

        return FILTER_PREFIX+retStr;
    }


    private static String passQbfParaValue(Object value){
        String retStr="";
        if(value instanceof Date)
            retStr= TypeUtils.dateUtcFormat((Date)value);
        else if(value instanceof Calendar)
            retStr= TypeUtils.dateUtcFormat((Calendar)value);
        else
            retStr= DataWriteUtils.getSQLValue(value);
        return retStr;
    }

    private static String qbfParameterFunc(String field,QBFCompareEnum compareEnum,String value){
        String retStr="";
        if(transformMap.containsKey(compareEnum))
           retStr=field+transformMap.get(compareEnum)+value;
        return retStr;
    }
}
