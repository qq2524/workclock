package clock.socoolby.com.clock.widget.textview.charanimator;

import android.graphics.Canvas;
import android.graphics.Paint;

import com.xenione.digit.TabConfig;
import com.xenione.digit.TabDigit;

public class TabDigitCharAnimator extends AbstractCharAnimator {

    TabDigit tabDigit;

    public TabDigitCharAnimator(String preString, String currentString, boolean up, boolean charDrawCenter, TabConfig tabConfig) {
        super(preString, currentString);
        tabDigit=new TabDigit(preString,currentString,up,charDrawCenter,tabConfig);
        tabDigit.start();
    }

    @Override
    public void drawCharPre(Canvas canvas, String strToDraw, float startX, float startY, Paint mTextPaint, float percent) {

    }

    @Override
    public void drawCharCurrent(Canvas canvas, String strToDraw, float startX, float startY, Paint mTextPaint, float percent) {
        canvas.save();
        canvas.translate(startX,startY-getFontHeight(mTextPaint)+mTextPaint.getFontMetrics().bottom);
        if(percent==0)
           tabDigit.setmNumberPaint(mTextPaint);
        tabDigit.onDraw(canvas);
        canvas.restore();
    }

    @Override
    public void move() {
        super.move();
        tabDigit.run();
    }

    @Override
    public boolean isCharAnimatorRuning() {
        return tabDigit.isRunning();
    }

    public static float getFontHeight(Paint paint) {
        Paint.FontMetrics fm = paint.getFontMetrics();
        return fm.bottom-fm.top;
    }

    public static float getFontBodyHeight(Paint paint) {
        Paint.FontMetrics fm = paint.getFontMetrics();
        return fm.descent - fm.ascent;
    }
}
