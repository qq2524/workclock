package clock.socoolby.com.clock.fragment.theme;

import clock.socoolby.com.clock.R;

public class ThemeUISampleFragment extends AbstractThemeUIFragment {

    public static final String THEME_NAME="sample";

    public ThemeUISampleFragment() {
        super(R.layout.theme_sample);
    }

    @Override
    void changeThemeTypeCheck() {
        themeUIViewModel.setThemeName(ThemeUIDefaultFragment.THEME_NAME);
    }
}
