package clock.socoolby.com.clock.fragment.houranimator;

import androidx.fragment.app.Fragment;

import clock.socoolby.com.clock.widget.animatorview.I_Animator;
import clock.socoolby.com.clock.widget.animatorview.animator.FerrisWheelAnimator;
import clock.socoolby.com.clock.widget.animatorview.animator.FishAnimator;
import clock.socoolby.com.clock.widget.animatorview.animator.WindmillAnimator;

public final class HourAnimatorFactory {
    public static final String DEFAULT="default";

    public static boolean isHourAnimator(String typeName){
        return !DEFAULT.equalsIgnoreCase(typeName);
    }

    public static Fragment builder(String typeName, int hour){
        I_Animator animator=null;
        switch (typeName){
            case FerrisWheelAnimator.NAME:
                break;
            case FishAnimator.NAME:
                animator=new FishAnimator(hour>12?hour-12:hour);
                break;
            case HourVideoFragment.NAME:
                return new HourVideoFragment(hour);
            default:
                animator=new WindmillAnimator(hour>12?hour-12:hour);
        }
        return new HourAnimatorFragment(animator);
    }
}
