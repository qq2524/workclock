package clock.socoolby.com.clock.net.listener;

public interface RequestListener<T> {
    void onResponse(T response);

    void onRequestFailed(int error, String errorMessage);
}
