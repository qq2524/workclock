package clock.socoolby.com.clock.widget.animatorview.animator;

import java.util.HashMap;
import java.util.Map;

import clock.socoolby.com.clock.widget.animatorview.AbstractAnimator;
import clock.socoolby.com.clock.widget.animatorview.animator.textanimator.EZLedAnimator;
import clock.socoolby.com.clock.widget.animatorview.animator.textanimator.EvaporateTextAnimator;

public class AnimatorFactory {

    public static Map<String,AnimatorEntry>  entryMap=new HashMap<>();

    public static Class[] animatorClassArray = new Class[]{
            StarFallAnimator.class,
            SkyAnimator.class,
            RainAnimator.class,
            SnowAnimator.class,
            BubbleWhirlPoolAnimator.class,
            BubbleAnimator.class,
            FluorescenceAnimator.class,
            BubbleCollisionAnimator.class,
            FireworkAnimator.class,
            DotsLineAnimator.class,
            WaterAnimator.class,
            FireAnimator.class,
            SawtoothAnimator.class,
            WindmillAnimator.class,
            VorolayAnimator.class,
            EZLedAnimator.class,
            EvaporateTextAnimator.class,
            PhaserBallAnimator.class,
            CarrouselAnimator.class,
            Wave3DAnimator.class,
            MagicLineAnimator.class,
            SnakeAnimator.class
    };

    static {
       for(Class c:animatorClassArray)
           addAnimatorEntryToMap(c);
    }

    public static void addAnimatorEntryToMap(Class animatorClass){
        addAnimatorEntryToMap(getInstance(animatorClass));
    }

    public static void addAnimatorEntryToMap(Class animatorClass,boolean randomColorAble){
        addAnimatorEntryToMap(getInstance(animatorClass,randomColorAble));
    }

    public static void addAnimatorEntryToMap(AnimatorEntry animatorEntry){
        entryMap.put(animatorEntry.type,animatorEntry);
    }

    public static AnimatorEntry getInstance(Class animatorClass){
         return getInstance(animatorClass,true);
    }

    public static AnimatorEntry getInstance(Class animatorClass,boolean randomColorAble){
         return new AnimatorEntry(animatorClass,randomColorAble);
    }

    public static class AnimatorEntry{
        String type;
        Class animatorClass;
        boolean randomColorAble;

        public AnimatorEntry(Class animatorClass) {
            this.animatorClass = animatorClass;
            this.type=animatorClass.getSimpleName();
            randomColorAble=true;
        }

        public AnimatorEntry(Class animatorClass, boolean randomColorAble) {
            this.animatorClass = animatorClass;
            this.randomColorAble = randomColorAble;
            this.type=animatorClass.getSimpleName();
        }
    }

    public static AbstractAnimator build(String name){
        AnimatorEntry entry=entryMap.get(name);
        if(entry==null)
            return null;
        AbstractAnimator ret=null;
        try {
            ret= (AbstractAnimator) entry.animatorClass.newInstance();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        return ret;
    }
}
