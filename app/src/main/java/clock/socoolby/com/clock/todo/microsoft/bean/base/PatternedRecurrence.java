package clock.socoolby.com.clock.todo.microsoft.bean.base;

import org.json.JSONException;
import org.json.JSONObject;

import clock.socoolby.com.clock.net.base.I_JsonObject;

public class PatternedRecurrence implements I_JsonObject {
    RecurrencePattern pattern;
    RecurrenceRange   range;

    @Override
    public void fromJson(JSONObject jsonObject) throws JSONException {
        JSONObject temp=jsonObject.getJSONObject("pattern");
        pattern=new RecurrencePattern();
        pattern.fromJson(temp);

        temp=jsonObject.getJSONObject("range");
        range=new RecurrenceRange();
        range.fromJson(temp);
    }

    @Override
    public void toJson(JSONObject jsonObject) throws JSONException {

    }
}
