package clock.socoolby.com.clock;

import com.openbravo.data.basic.BasicException;

import java.util.List;

import clock.socoolby.com.clock.dao.base.TimeFontStyle;
import e.odbo.data.dao.EntityManager;

public class FontManager {

    public static final TimeFontStyle DEFAULT_FONT_STYLE =new TimeFontStyle("ds_digi",140,150,160,200);

    private List<TimeFontStyle> fontStyleList;

    private TimeFontStyle currentFontStyle;

    private int currentFontIndex;

    EntityManager entityManager;

    public FontManager(EntityManager entityManager) {
        this.entityManager=entityManager;
        loadForDB();
        setCurrentFontName("ds_digi");
    }

    private void loadForDB(){
        try {
            fontStyleList=entityManager.list(TimeFontStyle.class);
        } catch (BasicException e) {
            e.printStackTrace();
        }
        if(fontStyleList==null||fontStyleList.size()==0)
            fontStyleList.add(DEFAULT_FONT_STYLE);
    }


    public int getCurrentFontSize(boolean isFullScreen,boolean isDisplaySecond){
        int fontSize;
        if(isFullScreen){
            if(isDisplaySecond) {
                fontSize=currentFontStyle.displaySecondOnFull;
            }else{
                fontSize=currentFontStyle.noDisplaySecondOnFull;
            }
        }else{
            if(isDisplaySecond) {
                fontSize=currentFontStyle.displaySecond;
            }else{
                fontSize=currentFontStyle.noDisplaySecond;
            }
        }
        return fontSize;
    }

    public int getMaxFontSize(boolean isDisplaySecond){
        int fontSize;
        if(isDisplaySecond) {
            fontSize=currentFontStyle.displaySecondOnFull;
        }else{
            fontSize=currentFontStyle.noDisplaySecondOnFull;
        }
        return fontSize;
    }

    public TimeFontStyle getCurrentFontStyle(){
        return currentFontStyle;
    }


    public TimeFontStyle nextFont(){
        if(currentFontIndex <fontStyleList.size()-1) {
            currentFontIndex++;
        }else
            currentFontIndex =0;
        currentFontStyle=fontStyleList.get(currentFontIndex);
        return currentFontStyle;
    }

    public void setCurrentFontName(String name){
        int index=0;
        for(TimeFontStyle entry:fontStyleList) {
            if (entry.name.equalsIgnoreCase(name)) {
                currentFontStyle = entry;
                currentFontIndex=index;
            }
            index++;
        }
    }

    private TimeFontStyle getFontStyleByName(String name){
        for(TimeFontStyle entry:fontStyleList)
            if (entry.name.equalsIgnoreCase(name)) {
                return entry;
            }
        return null;
    }

    public void updateCurrentFontSize(boolean isFullScreen,boolean isDisplaySecond,int newFontSize){
        if(isFullScreen){
            if(isDisplaySecond) {
                currentFontStyle.displaySecondOnFull=newFontSize;
            }else{
                currentFontStyle.noDisplaySecondOnFull=newFontSize;
            }
        }else{
            if(isDisplaySecond) {
                currentFontStyle.displaySecond=newFontSize;
            }else{
                currentFontStyle.noDisplaySecond=newFontSize;
            }
        }
        try {
            entityManager.update(currentFontStyle);
        } catch (BasicException e) {
            e.printStackTrace();
        }
    }

    public void updateFontSize(String fontName,boolean isFullScreen,boolean isDisplaySecond,int newFontSize){
        TimeFontStyle timeFontStyle=getFontStyleByName(fontName);
        if(timeFontStyle==null)
            return;
        if(isFullScreen){
            if(isDisplaySecond) {
                timeFontStyle.displaySecondOnFull=newFontSize;
            }else{
                timeFontStyle.noDisplaySecondOnFull=newFontSize;
            }
        }else{
            if(isDisplaySecond) {
                timeFontStyle.displaySecond=newFontSize;
            }else{
                timeFontStyle.noDisplaySecond=newFontSize;
            }
        }
        try {
            entityManager.update(timeFontStyle);
        } catch (BasicException e) {
            e.printStackTrace();
        }
    }

    public void addCoustomFontStyle(List<TimeFontStyle> coustomFontStyleList){
        this.fontStyleList.addAll(coustomFontStyleList);
    }

    public void addCoustomFontStyle(TimeFontStyle coustomFontStyleList){
        this.fontStyleList.add(coustomFontStyleList);
    }
}
