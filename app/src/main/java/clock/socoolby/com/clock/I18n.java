package clock.socoolby.com.clock;

import android.content.Context;

import clock.socoolby.com.clock.state.ClockInterfaceTypeEnum;
import clock.socoolby.com.clock.state.ClockModeEnum;

public class I18n {
    public static  Context appContext;

    public static void init(Context context){
        appContext=context;
    }

    public static final String get(ClockModeEnum clockModeEnum){
        return appContext.getString(R.string.about);
    }

    public static final String get(ClockInterfaceTypeEnum clockInterfaceTypeEnum){
        return null;
    }
}
