package clock.socoolby.com.clock.cache;


import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class MemoryTrashCache<Q> implements I_TrashCache<Q> {

    protected Queue<Q> trashCache;

    public MemoryTrashCache() {
        trashCache = new LinkedList<>();
    }

    public void moveToTrashCache(List<Q> objs){
        for(Q obj:objs)
            trashCache.offer(obj);
    }

    public void moveToTrashCache(Q obj){
        trashCache.offer(obj);
    }

    public Q revectForTrashCache(){
        //Log.d("CacheDifferenceAnimator","revectForTrashCache,current size:"+trashCache.size());
        if(trashCache.size()>0)
            return trashCache.poll();
        return null;
    }

    public int getTrashCacheSize(){
        return trashCache.size();
    }

    @Override
    public void clear() {
        trashCache.clear();
    }

}
