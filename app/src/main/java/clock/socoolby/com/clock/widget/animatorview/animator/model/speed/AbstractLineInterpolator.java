package clock.socoolby.com.clock.widget.animatorview.animator.model.speed;


public abstract class AbstractLineInterpolator extends AbstractInterpolator<Float> {

    public AbstractLineInterpolator(Float base, Float maxValue) {
        super(base, maxValue);
    }

    @Override
    public Float calc() {
       return calculate(currentValue-base,base,maxValue-currentValue,maxValue);
    }

    public abstract Float calculate(float timeGo, float base, float lenght, float totalTime);

}
