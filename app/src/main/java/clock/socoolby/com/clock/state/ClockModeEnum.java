package clock.socoolby.com.clock.state;

public enum ClockModeEnum {
    NORMAL(0),HANDUP(1),DELAY(2),ALTER(3);

    public  int code;

    ClockModeEnum(int code) {
        this.code = code;
    }

    public static ClockModeEnum valueOf(int code){
        switch (code){
            case 1:
                return HANDUP;
            case 2:
                return DELAY;
            case 3:
                return ALTER;
            default:
                return NORMAL;
        }
    }
}
