package clock.socoolby.com.clock.widget.animatorview;

import android.graphics.Point;

public interface I_Trajectory {

    //进行移动取x点值
    Point nextStep(int prevX, int prevY);

    //旋转轨道
    void rotate(int angle);

    //从当前位置移动到新的
    void transfer(int newX,int newY);

    //反转轨道
    void reverse();
}
