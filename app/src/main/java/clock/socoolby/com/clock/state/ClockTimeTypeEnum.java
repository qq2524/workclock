package clock.socoolby.com.clock.state;

public enum ClockTimeTypeEnum {
    TIME(0),COUNTING_DOWN(1),COUNTING(2);

    public int code;

    ClockTimeTypeEnum(int code) {
        this.code = code;
    }

    public static ClockTimeTypeEnum valueOf(int code){
        switch (code){
            case 1:
                return COUNTING_DOWN;
            case 2:
                return COUNTING;
            default:
                return TIME;
        }
    }
}
