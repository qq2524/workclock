package clock.socoolby.com.clock.todo.microsoft;


import clock.socoolby.com.clock.net.listener.AbstractEntityRequestListener;
import clock.socoolby.com.clock.net.listener.RequestListener;
import clock.socoolby.com.clock.todo.microsoft.bean.todo.TodoEntity;

public  class EntityListenerFactory {

    public static AbstractEntityRequestListener<TodoEntity> TODO_Entity(RequestListener<TodoEntity> warpListener){
        return new AbstractEntityRequestListener<TodoEntity>(warpListener) {
            @Override
            public TodoEntity creatEntity() {
                return new TodoEntity();
            }
        };
    }
}
