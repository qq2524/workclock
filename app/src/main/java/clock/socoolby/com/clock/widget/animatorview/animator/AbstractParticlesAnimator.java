package clock.socoolby.com.clock.widget.animatorview.animator;

import clock.socoolby.com.clock.cache.I_TrashCache;
import clock.socoolby.com.clock.widget.animatorview.AbstractCacheAbleAnimator;

public abstract class AbstractParticlesAnimator extends AbstractCacheAbleAnimator<FluorescenceAnimator.Particle> {


    public AbstractParticlesAnimator(int entryQuantity) {
        super(entryQuantity);
    }

    public AbstractParticlesAnimator(int entryQuantity, I_TrashCache<FluorescenceAnimator.Particle> trashCache) {
        super(entryQuantity, trashCache);
    }
}
